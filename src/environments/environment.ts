// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyDwPAc2-BlaB9HlomPeZNDyLanT5t4l4og",
    authDomain: "example-adida.firebaseapp.com",
    projectId: "example-adida",
    storageBucket: "example-adida.appspot.com",
    messagingSenderId: "707957422499",
    appId: "1:707957422499:web:d8df994bb9785c9e6f0216"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
