import { Client } from './../interfaces/client';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'clientform',
  templateUrl: './clients-form.component.html',
  styleUrls: ['./clients-form.component.css']
})
export class ClientsFormComponent implements OnInit {

  @Input() name:string;
  @Input() years:number;
  @Input() salary:number;
  @Input() id:string;
  @Input() formType:string;
  @Output() update = new EventEmitter<Client>()
  @Output() closeEdit = new EventEmitter<null>()
  isError:boolean = false;

  tellParentToClose(){
    this.closeEdit.emit();
  }

  updateParent(){
    let client:Client = {id:this.id, name:this.name, years:this.years, salary:this.salary};
    if(this.years < 0 || this.years > 24){
      this.isError = true;
    }
    else{
    this.update.emit(client);
    if(this.formType == "Add Client"){
      this.name = null;
      this.years = null;
      this.salary = null;
    }
   }
  }

  constructor() { }

  ngOnInit(): void {
  }

}
